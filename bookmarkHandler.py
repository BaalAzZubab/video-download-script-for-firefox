import sqlite3
import os
import configparser
import sys
from shutil import copyfile as cp

def find_firefox(HOME_PATH):
    if(sys.platform == 'darwin'):
        return HOME_PATH + 'Library/Application Support/Firefox/'
    elif (platform() == 'linux'):
        return HOME_PATH + '.mozilla/firefox/'
            #throw some error
    else:
        return ""

def find_firefox_profiledir(FF_PROFILE, FF_DIR):
    profiles = configparser.ConfigParser()
    profiles.read(FF_DIR + 'profiles.ini')
    for prf in profiles.sections():
        if profiles[prf]['Name'] == FF_PROFILE:
            return (profiles[prf]['Path'] + '/')
    # throw some errors if not found

def get_bookmarks(WORKING_DIR, FF_DIR, FF_PROFILE_DIR):
    print('Updating the database...')
    cp( FF_DIR + FF_PROFILE_DIR + 'places.sqlite', WORKING_DIR + 'places.sqlite')

    # establish connection to a copy of the database and create the cursor
    connection = sqlite3.connect('places.sqlite')
    c = connection.cursor()

    # extract a list of all bookmark-urls
    c.execute('''
            SELECT moz_places.url FROM moz_bookmarks, moz_places
            WHERE moz_bookmarks.title='wl'
            AND moz_bookmarks.fk = moz_places.id
            ;''')

    # store the urls
    bookmarks = c.fetchall()
    # close the connection - not needed anymore
    connection.close()
    print('Received all bookmarks.\nExtracting the urls...')
    return bookmarks

def download_bookmarks(bookmarks, OUT_DIR, OUT_FORMAT):
    # iterate over the list of bookmarks and download the videos
    print('Downloading...')
    count = 0
    print('{0}/{1}'.format(count, len(bookmarks)))
    for thing in bookmarks:
        os.system("youtube-dl -q --ignore-config --no-playlist -o \"{0}{1}\" {2}".format(OUT_DIR, OUT_FORMAT, thing[0]))
        count += 1
        print('{0}/{1}'.format(count, len(bookmarks)))
    print("success")

def main():

    # initialize variables and set path variables
    HOME_PATH = os.environ['HOME'] + '/'
    WORKING_DIR = os.path.dirname(os.path.realpath(__file__)) + '/'

    if len(sys.argv) == 1:
        cfg_profile = 'DEFAULT'
    elif len(sys.argv) == 2:
        cfg_profile = sys.argv[1]
    else:
        cfg_profile = sys.argv.pop()
        main()

    print('Handling profile {}...'.format(cfg_profile))

    # read config file
    config = configparser.ConfigParser(interpolation = None)
    config.read(WORKING_DIR + '.config.ini')
    FF_PROFILE = config[cfg_profile].get('firefoxprofile')
    OUT_DIR = config[cfg_profile].get('outputdirectory')
    OUT_FORMAT = config[cfg_profile].get('outputformat')

    # setting os specific paths
    FF_DIR = find_firefox(HOME_PATH)
    FF_PROFILE_DIR = find_firefox_profiledir(FF_PROFILE, FF_DIR)

    # get the videos
    bookmarks = get_bookmarks(WORKING_DIR, FF_DIR, FF_PROFILE_DIR)
    download_bookmarks(bookmarks, OUT_DIR, OUT_FORMAT)

if(__name__ == '__main__'):
    main()
