# video download script for firefox

This is a little script that reads from your firefox' bookmarks folder all urls with the name wl (watch later) and hands them over to youtube-dl to download them

## Install

+ Just clone it somewhere and eventually add an alias to run it from anywhere

#### Dependencies

+ python3
	+ os
	+ shutils
	+ sqlite
	+ ConfigParser
	+ sys
+ firefox
+ youtube-dl


## Usage

+ add the videos you want do download as bookmark named 'wl'
+ run the script ('python3 bookmarkhandler.py')
+ remove the bookmarks manually from firefox

## TODO

+ add multi-profile support of the config file
+ script removes the downloaded bookmarks automatically
